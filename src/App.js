import React from "react";
import Input from "./components/Input";

function App() {
  return (
    <div className="App">
      <h1>Employee Registration Form</h1>
      <hr />
      <Input />
    </div>
  );
}

export default App;
