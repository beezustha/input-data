import React from "react";


function Input() {
  function handleClick(){
    
  }

  return (
    <div>
      <h2>Enter your information </h2>
      <div className="input-section">
        <form>
          <h3>Name</h3>
          <input type="text" name="name" placeholder="Enter your name" />
          <h3>Gender</h3>
          <label>
            <input type="checkbox" placeholder="male" />
            Male
          </label>
          <label>
            <input type="checkbox" placeholder="female" />
            Female
          </label>
          <h3>Email</h3>
          <input type="text" name="email" placeholder="Email Address" />
          <h3>Contact no.</h3>
          <input type="text" name="contact" placeholder="Contact Number" />
          <h3>Address</h3>
          <input type="text" name="address" placeholder="Location" />
          <h3>Position</h3>
          <input type="text" name="position" placeholder="Job position" />
          <h3>Experience</h3>
          <input type="text" name="experience" placeholder="Experience" />
          <button onClick={handleClick}>Add</button>
          <h3>Education</h3>
          <input type="text" name="education" placeholder="Education" />
        </form>
      </div>
    </div>
  );
}

export default Input;
